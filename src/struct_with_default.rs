macro_rules! _default {
    ($default:expr) => { $default };
    () => { ::std::default::Default::default() };
}

macro_rules! struct_with_default {
    (
        $(
            #[$meta:meta]
        )*
        $(struct)? $type:ident {
            $($field:ident: $fieldtype:ty $(= $default:expr)? $(,)?)*
        }
    ) => {
        $(#[$meta])*
        struct $type {
            $($field: $fieldtype),
            *
        }
        impl Default for $type {
            fn default() -> Self {
                $type {$(
                    $field: _default!($($default)?),
                )*}
            }
        }
    };
}